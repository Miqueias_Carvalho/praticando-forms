import "./style.css";

function Card(props) {
  const {
    checkbox,
    email,
    emailConfirm,
    name,
    password,
    passwordConfirm,
    phone,
    user,
  } = props.dadosUsuario;

  return (
    <div className="informacoes">
      <p>
        Check: <span>{checkbox.toString()}</span>{" "}
      </p>
      <p>
        Usuário: <span>{user}</span>
      </p>
      <p>
        Nome: <span>{name}</span>
      </p>
      <p>
        Fone: <span>{phone}</span>
      </p>
      <p>
        Email: <span>{email}</span>
      </p>
      <p>
        Confirmação: <span>{emailConfirm}</span>{" "}
      </p>
      <p>
        Senha: <span>{password}</span>
      </p>
      <p>
        Confirmação: <span>{passwordConfirm}</span>
      </p>
    </div>
  );
}
export default Card;
