import "./App.css";
import { useState } from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import Card from "./components/Card";

function App() {
  const [dadosUsuario, setDadosUsuario] = useState();

  const formSchema = yup.object().shape({
    user: yup
      .string()
      .required("nome de usuário obrigatório")
      .max(18, "maximo 18 caracteres"),
    name: yup
      .string()
      .required("nome obrigatório")
      .max(18, "maximo 18 caracteres"),
    email: yup.string().required("email obrigatório").email("email inválido"),
    emailConfirm: yup
      .string()
      .required("email obrigatório")
      .email("email inválido")
      .oneOf([yup.ref("email")], "email diferente"),
    phone: yup
      .string()
      .required("telefone obrigatório")
      .min(9, "minimo 8 digitos e o ' - ': 0000-0000 ")
      .matches(/(\(?\d{2}\)?\s)?(\d{4,5}\-\d{4})/, "telefone inválido"),
    password: yup
      .string()
      .required("senha obrigatória")
      .min(8, "mínimo 8 caracteres")
      .matches(
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[$*&@#])[0-9a-zA-Z$*&@#]{8,}$/,
        "números, letras maiusculas,minúsculas e caracter especial"
      ),
    passwordConfirm: yup
      .string()
      .required("senha errada")
      .oneOf([yup.ref("password")], "senha_diferente"),
    checkbox: yup.boolean().required(),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const onSubmitFunction = (data) => {
    console.log(data);
    setDadosUsuario(data);
  };

  // console.log(errors);

  return (
    <div className="App">
      <main className="App-container">
        <form className="formulario" onSubmit={handleSubmit(onSubmitFunction)}>
          <div className="entrada_de_dados">
            <input
              type="text"
              placeholder="Nome de Usuário*"
              {...register("user")}
            />
            <p className="entrada_de_dados_error_message">
              {errors.user && `* ${errors.user.message}`}
            </p>
          </div>

          <div className="entrada_de_dados">
            <input
              type="text"
              placeholder="Nome Completo*"
              {...register("name")}
            />
            <p className="entrada_de_dados_error_message">
              {errors.name && `* ${errors.name.message}`}
            </p>
          </div>

          <div className="entrada_de_dados">
            <input
              type="text"
              placeholder="Endereço de Email*"
              {...register("email")}
            />
            <p className="entrada_de_dados_error_message">
              {errors.email && `* ${errors.email.message}`}
            </p>
          </div>

          <div className="entrada_de_dados">
            <input
              type="text"
              placeholder="Confirme seu Email*"
              {...register("emailConfirm")}
            />
            <p className="entrada_de_dados_error_message">
              {errors.emailConfirm && `* ${errors.emailConfirm.message}`}
            </p>
          </div>

          <div className="entrada_de_dados">
            <input
              type="text"
              placeholder="(xx) 0000-0000*"
              {...register("phone")}
            />
            <p className="entrada_de_dados_error_message">
              {errors.phone && `* ${errors.phone.message}`}
            </p>
          </div>

          <div className="entrada_de_dados senha">
            <div className="container_senhas">
              <input
                type="password"
                placeholder="Senha"
                {...register("password")}
              />
              <input
                type="password"
                placeholder="Confirme sua senha*"
                {...register("passwordConfirm")}
                className={
                  errors.passwordConfirm
                    ? errors.passwordConfirm.message
                    : "none"
                }
              />
            </div>

            <div className="container_erros">
              <p className="entrada_de_dados_error_message">
                {errors.password && `* ${errors.password.message}`}
              </p>
              {/* <p className="entrada_de_dados_error_message">
                {errors.passwordConfirm &&
                  `* ${errors.passwordConfirm.message}`}
              </p> */}
            </div>
          </div>

          <div className="entrada_de_dados checkbox">
            <input type="checkbox" id="check" {...register("checkbox")} />
            <label htmlFor="check">
              Eu aceito os termos de uso da aplicação
            </label>
          </div>
          <div className="entrada_de_dados submit_container">
            <button type="submit">CADASTRAR</button>
            <p>Já possui uma conta ?</p>
          </div>
        </form>
        {dadosUsuario && <Card dadosUsuario={dadosUsuario} />}
      </main>
    </div>
  );
}

export default App;
